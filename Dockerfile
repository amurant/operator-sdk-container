FROM golang:1.14-alpine AS go

FROM docker:19.03

COPY --from=go /usr/local/go /usr/local/go

ENV GOPATH /go
ENV PATH $GOPATH/bin:/usr/local/go/bin:$PATH

RUN mkdir -p "$GOPATH/src" "$GOPATH/bin" && chmod -R 777 "$GOPATH"
WORKDIR $GOPATH

RUN apk update \
    && apk upgrade \
    && apk add --no-cache curl git make ca-certificates

ARG RELEASE_VERSION=v0.16.0

RUN curl -OJL https://github.com/operator-framework/operator-sdk/releases/download/${RELEASE_VERSION}/operator-sdk-${RELEASE_VERSION}-x86_64-linux-gnu \
    && chmod +x operator-sdk-${RELEASE_VERSION}-x86_64-linux-gnu \
    && cp operator-sdk-${RELEASE_VERSION}-x86_64-linux-gnu /usr/local/bin/operator-sdk \
    && rm operator-sdk-${RELEASE_VERSION}-x86_64-linux-gnu

ENV GO111MODULE=on

CMD ["operator-sdk"]